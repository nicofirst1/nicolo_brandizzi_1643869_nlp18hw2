import datetime
import os
import random
import tensorflow as tf
import numpy as np
from termcolor import colored
import pickle


def perc_print(current, total, msg="", inverted=False):
    if msg:
        msg += ": "

    to_print = current / total

    if inverted:
        to_print = 1 - to_print

    to_print = round(to_print * 100, 2)
    print(colored(f"\r{msg}{to_print}% ", "yellow"), end="")

    return round(to_print)


def get_file_lines(file_path):
    """
    get the number of lines in a file
    :param file_path: the path to the file
    :return: int, number of lines
    """

    lines = 0

    with open(file_path, "r+") as file:
        for line in file:
            lines += 1

    return lines


def remove_lines_from_file(file_path, line_indices):
    """
    Remove some lines from a file
    :param file_path: the file path
    :param line_indices: a list of line indices to be removed
    :return:
    """

    lines = []
    idx = 0

    print(f"removing lines: {line_indices}")

    with open(file_path, "r+") as file:

        for line in file:
            if idx not in line_indices:
                lines.append(line)
            idx += 1

    with open(file_path, "w+") as file:
        file.writelines(lines)


def save_structure(struct, file_name):
    with open(file_name, "wb") as file:
        pickle.dump(struct, file)

    print(f"Saved {file_name}")


def load_struct(file_path):
    try:
        with open(file_path, "rb") as file:
            print(f"Loading {file_path}...")
            return pickle.load(file)
    except FileNotFoundError:
        print("Could not find the specified file")
        return -1


def look_in_babelfy(look_word):
    """
    Look for a word in the original babelfy document
    :param look_word: the word to look for in the format 'word:id'
    :return: None
    """

    file_path = "../DataSets/HW2/babelfy_vectors"

    lines = 3849894  # the file has this number of lines
    idx = 0

    # bring the word into the file form
    if ':' in look_word:
        look_word = look_word.split(':')[0] + "_bn:" + look_word.split(':')[1]
    found = False

    with open(file_path, "r+") as file:

        for line in file:

            word = line.split()[0]
            if look_word == word:
                print(f"found {word} at line {idx}")
                print(line)
                found = True
                break

            perc_print(idx, lines)
            idx += 1

    if not found:
        print(f"\nword : {look_word} not found")


def empty_dir(log_dir_path):
    print(f"Emptying {log_dir_path}")
    for file in os.listdir(log_dir_path):
        os.remove(log_dir_path + file)


def f1_score(y_true, y_pred):
    correct_asw = 0.0
    given_asw = 0.0
    total_asw = 0.0

    correct_asw += len([1 for p, t in zip(y_pred, y_true) if p == t])
    given_asw += len([elem for elem in y_pred if elem])
    total_asw += len(y_pred)

    recall = correct_asw / total_asw
    precision = correct_asw / given_asw
    try:
        f1 = 2 * recall * precision / (recall + precision)
    except ZeroDivisionError:
        f1 = 0

    print(colored(f"precision: {precision}, recall: {recall}, f1_score:{f1}", color="green"))

    return f1


def analyze_incorrect_meanings(y_pred, y_true, id2label, meaning_dict, right_words=False):
    """
    Print useful infos on the prediction
    :param y_pred: the prediction array
    :param y_true: the true array values
    :param id2label : a dictionary which maps ids to babelnet labels
    :param meaning_dict: a dict which maps a lemma to a list of babelnet labels
    :return: None
    """

    prov = [(id2label[p], id2label[t]) for p, t in zip(y_pred, y_true) if p != t]

    errors = 0
    semi_correct = 0

    for p, t in prov:

        for k, v in meaning_dict.items():

            if p in v and t not in v:
                errors += 1
                break

            elif t in v and p not in v:
                errors += 1
                break
            elif p in v and t in v:
                semi_correct += 1
                break

    to_print = colored(
        f"Out of {len(prov)} incorrect ones, {errors} ({round(errors/len(prov)*100,2)}%) are from different words, while {semi_correct} ({round(semi_correct/len(prov)*100,2)}%) are wrong meaning from the same word",
        color="green")
    print(to_print)

    if right_words:
        prov = [(id2label[p], id2label[t]) for p, t in zip(y_pred, y_true) if p == t]
        right_words = []
        for p, t in prov:

            for k, v in meaning_dict.items():

                if p in v and t in v:
                    right_words.append(k)

        right_words = list(set(right_words))
        random.shuffle(right_words)

        if len(right_words) > 5:
            right_words = right_words[:5]

        print(f"Some correct words are the following:\n{right_words}")


def time_print(time_in_seconds):
    return str(datetime.timedelta(seconds=time_in_seconds))


def tesor_analizer(tensor):
    nn_z = 0
    total = 0

    for i in tensor:

        for j in i:

            indices = np.nonzero(j)[0]
            if len(indices) > 0:
                nn_z += 1
                print(len(indices))
            total += 1

    print(nn_z)
    print(total)


def apply_meaning_tensor(scores, meaning_tensor):

    for i in range(len(scores)):
        for j in range(len(scores[i])):
            scores[i][j]=np.multiply(scores[i][j],meaning_tensor[i][j])

    return scores


def apply_most_common_senses(scores, meaning_batch, threshold,y_true=None, debug=True):
    new_scores=np.zeros(shape=meaning_batch.shape)

    most_common_used=0.0
    softmax_used=0.0
    incorrect_common=0.0
    total=0

    #random_prob=1.0/scores.shape[-1]
    #threshold=random_prob+threshold*random_prob

    for i in range(len(scores)):
        for j in range(len(scores[i])):
            max_v=np.amax(scores[i][j])

            if max_v==0:
                new_scores[i][j]=max_v

            elif max_v>threshold:
                new_scores[i][j]=np.argmax(scores[i][j])
                softmax_used+=1
                total+=1

                #count the number of times the softmax is the most common and the result is incorrect
                if y_true is not None:
                    if np.argmax(scores[i][j])!=y_true[i][j] and np.argmax(scores[i][j])==meaning_batch[i][j]:
                        incorrect_common+=1


            else:
                new_scores[i][j]=meaning_batch[i][j]
                most_common_used+=1
                total+=1

    if debug:
        to_print=f"out of {total}," \
                 f" {most_common_used} ({round(most_common_used/total*100,2)}%) are most common," \
                 f" {softmax_used} ({round(softmax_used/total*100,2)}%) are from softamx," \
                 f" while {incorrect_common} ({round(incorrect_common/softmax_used*100,2)}%) are incorrect common"
        print(to_print)
    return new_scores, total,most_common_used,softmax_used, incorrect_common



def play_soud(duration=1, freq=500, repeat=1):
    """
    Play a sound
    :param duration: duration in seconds
    :param freq: frequence of the sound
    :return:
    """

    for i in range(repeat):
        os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))


def normalize_tensors(base, to_norm):

    base=tf.to_float(base)
    to_norm=tf.to_float(to_norm)

    concat = tf.stack([base, to_norm],axis=0)

    concat = tf.div(
        tf.subtract(
            concat,
            tf.reduce_min(concat)
        ),
        tf.subtract(
            tf.reduce_max(concat),
            tf.reduce_min(concat)
        )
    )

    return concat[1]

def get_dict_value(dictionary, search_key, error_return):

    try:
        return dictionary[search_key]
    except KeyError:
        return error_return