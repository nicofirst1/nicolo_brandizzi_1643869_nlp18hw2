import random
import xml.etree.ElementTree as etree
from collections import Counter

import numpy as np
import scipy
from  scipy.sparse import csr_matrix
from data_util.parser import parse_lemma_prob_matrix
from utils import save_structure, load_struct, get_dict_value


class DataBatch:
    """
    A class for data parsing/manipulation

    Attributes:
        data_param: (class) the DataParam class to hold parameters about data managing

        Train Variables:
            train_xml (dict):  a dictionaries where each key is a document (ex. d000) and each value is a list of sentences associated with it
            train_keY (dict): maps sentence ids (d000.s000,..., d011.s001) to a babelnet id
            train_len (int): the updated length of all the sentences in all the documents

        Eval Variables:
            eval_xml (dict): a dictionaries where each key is a document (ex. d000) and each value is a list of sentences associated with it
            eval_keY (dict): maps sentence ids (d000.s000,..., d011.s001) to a babelnet id
            eval_len (int): the updated length of all the sentences in all the documents
            corpus_name (str): the current corpus of the evaluation task (senseval2,...., semeval2015)

        Generic Variables:
            word2id (dict): maps a word to a meaning id
            id2word (dict): maps an id to a word meaning
            label2id (dict): maps a label to an id
            id2label (dict): maps an id to a label
            vocabulary_size (int): the number of word in the whole training set
            meaning_dict (dict): a dict in which a key is a word and the value is a list of associated babelnet meaning ids for that word
            label_num (int): the number of labels (word senses)



    """

    def __init__(self, data_param, use_eval):
        """

        :param data_param:  (class) the DataParam class to hold parameters about data managing
        """

        self.params = data_param
        self.use_eval = use_eval


        ##TRAIN VARIABLES
        self.train_xml = self.parse_train_xml()
        self.train_key = self.get_key_dict(True)
        self.train_len = sum([len(elem) for elem in self.train_xml.values()])

        ##EVAL VARIABLES
        self.eval_xml = self.parse_eval_xml()
        self.eval_key = self.get_key_dict(False)
        self.eval_len = sum([len(elem) for elem in self.eval_xml.values()])
        self.corpus_name = list(self.eval_xml.keys())[0]

        ##POS VARIABLES
        self.pos2id, self.id2pos = self.parse_pos(use_eval)
        self.pos_size = len(self.pos2id)

        ##TEST VARIABLES
        self.test_xml = self.parse_test()
        self.test_len = len(self.test_xml)
        self.save_test_file=False

        # Generic variables
        self.word2id, self.id2word = self.get_word_dict()
        self.label2id, self.id2label = self.get_label_id_dict()
        self.word2label, self.label2word = self.get_label_word_dict()
        self.vocabulary_size = len(self.word2id)
        self.meaning_dict = parse_lemma_prob_matrix(self.params.train_key, self.params.train_xml)
        self.label_num = len(self.label2id)

        self.most_common_senses=load_struct(self.params.most_common_senses)
        #self.most_common_senses = -1
        if self.most_common_senses == -1:
            self.most_common_senses = self.parse_most_common_senses(use_eval=use_eval)
            save_structure(self.most_common_senses, self.params.most_common_senses)

    def set_train(self):
        """
        Set the train for the next epoch
        """

        self.train_xml = self.parse_train_xml()
        self.train_len = sum([len(elem) for elem in self.train_xml.values()])

    def set_eval(self):
        """
        Set the eval for the next epoch
        """
        self.eval_xml = self.parse_eval_xml()
        self.eval_len = sum([len(elem) for elem in self.eval_xml])
        self.corpus_name = list(self.eval_xml.keys())[0]

    # ----------GETTER

    def get_key_dict(self, train):
        """
        Get the key dictionary
        :param train: (bool), which of the key file to open
        :return: a dictionary in wichh the key is an identifier (i.e. d000.s000) and the value a babelnet id
        """

        key_dict = {}

        # ge thte right path
        if train:

            res = load_struct(self.params.train_key_res)
            if res != -1:
                return res

            file_path = self.params.train_key
        else:

            res = load_struct(self.params.eval_key_res)
            if res != -1:
                return res

            file_path = self.params.eval_key

        # open the file
        with open(file_path, "r+") as file:

            # for every line use append the key and value
            for line in file:
                line = line.split()

                key_dict[line[0]] = line[1].split(":")[1]

        if train:
            save_structure(key_dict, self.params.train_key_res)
        else:
            save_structure(key_dict, self.params.eval_key_res)

        return key_dict

    def get_word_dict(self):
        """
        Get the word2id and id2word from the train data
        :return: the to dictioanries
        """

        word2id = load_struct(self.params.word2id)
        id2word = load_struct(self.params.id2word)

        if word2id != -1 and id2word != -1:
            return word2id, id2word


        train_corpus, eval_corpus=self.generate_eval_train_sentence_list()

        if self.use_eval:
            train_corpus+=eval_corpus
        words = []

        # append every word found in the train xml file

        for sent in train_corpus:

            for word in sent:
                words.append(word.text)

        # remoe duplicates
        words = list(set(words))

        # initialize dictionaries with unk
        word2id = {'unk': 0}
        id2word = {0: 'unk'}

        # add the words
        for word in words:
            word2id[word] = len(word2id)
            id2word[len(id2word)] = word

        save_structure(word2id, self.params.word2id)
        save_structure(id2word, self.params.id2word)

        return word2id, id2word

    def get_label_id_dict(self):
        """
        Generate two dictionaries  for the babelnet ids
        :return:
            label2id: map babel id to int
            id2label: map int to babel id
        """

        label2id = load_struct(self.params.label2id)
        id2label = load_struct(self.params.id2label)

        if label2id != -1 and id2label != -1:
            return label2id, id2label

        label2id = {'unk': 0}
        id2label = {0: 'unk'}

        ids = list(set([elem for elem in self.train_key.values()]))
        for babel_id in ids:
            label2id[babel_id] = len(label2id)
            id2label[len(id2label)] = babel_id

        save_structure(label2id, self.params.label2id)
        save_structure(id2label, self.params.id2label)

        return label2id, id2label

    def get_label_word_dict(self):

        word2label = load_struct(self.params.word2label)
        label2word = load_struct(self.params.label2word)

        if word2label != -1 and label2word != -1:
            return word2label, label2word

        train_sentences, eval_sentences = self.generate_eval_train_sentence_list()

        if self.use_eval:
            train_sentences += eval_sentences

        word2label = {}
        label2word = {}

        for sentence in train_sentences:

            for word in sentence:

                if 'id' not in word.attrib.keys():
                    continue

                try:
                    meaning = self.train_key[word.attrib['id']]
                except KeyError:
                    meaning = self.eval_key[word.attrib['id']]

                if meaning not in word2label.values():
                    word2label[word.text] = meaning
                    label2word[meaning] = word.text

        save_structure(word2label, self.params.word2label)
        save_structure(label2word, self.params.label2word)

        return word2label, label2word

    # ----------PARSER
    def parse_eval_xml(self):
        """
        Split the eval per corpus (senseval1/2..)
        :return:
        dict where the keys are the corpus names and the value is a list of sentences associated with the key
        """
        print("\nParsing xml eval file....")

        corpus_dict = load_struct(self.params.eval_xml_res)
        if corpus_dict != -1:
            return corpus_dict

        corpus_dict = {}
        corpus = etree.parse(self.params.eval_xml).getroot()

        for document in corpus:

            for sentence in document:

                senseval = sentence.attrib['id'].split(".")[0]

                if senseval not in corpus_dict.keys():
                    corpus_dict[senseval] = []

                corpus_dict[senseval].append(sentence)

        save_structure(corpus_dict, self.params.eval_xml_res)

        return corpus_dict

    def parse_train_xml(self):
        """
        Parse the xml file and return the following structure
        :return: a dictionary in which the keys are the various documents and the values
                are the xml sentences objects
        """

        print("\nParsing xml train file....")

        document_dict = load_struct(self.params.train_xml_res)
        if document_dict != -1:
            return document_dict

        document_dict = {}

        corpus = etree.parse(self.params.train_xml).getroot()

        # for every document in the corpus
        for document in corpus:

            # for every sentence in the document
            for sentence in document:

                document = sentence.attrib['id'].split(".")[0]

                if document not in document_dict.keys():
                    document_dict[document] = []

                document_dict[document].append(sentence)

        save_structure(document_dict, self.params.train_xml_res)

        return document_dict

    def parse_most_common_senses(self, use_eval):

        senses_dict = {}

        train_sentences, eval_sentences = self.generate_eval_train_sentence_list()

        if use_eval:
            for sentence in eval_sentences:

                for word in sentence:
                    if 'id' in word.attrib.keys():
                        if word.attrib['lemma'] not in senses_dict.keys():
                            senses_dict[word.attrib['lemma']] = []

                        senses_dict[word.attrib['lemma']].append(self.eval_key[word.attrib['id']])

        for sentence in train_sentences:

            for word in sentence:
                if 'id' in word.attrib.keys():
                    if word.attrib['lemma'] not in senses_dict.keys():
                        senses_dict[word.attrib['lemma']] = []

                    senses_dict[word.attrib['lemma']].append(self.train_key[word.attrib['id']])

        final_dict = {}
        for k, v in senses_dict.items():
            most_common = Counter(v).most_common()
            final_dict[k] = most_common[0][0]

        return final_dict

    def parse_pos(self, use_eval):

        pos2id = load_struct(self.params.pos2id)
        id2pos = load_struct(self.params.id2pos)

        if pos2id != -1 and id2pos != -1:
            return pos2id, id2pos

        # get the sentences
        train_sentences, eval_sentences = self.generate_eval_train_sentence_list()

        total_list = train_sentences

        if use_eval:
            total_list += eval_sentences

        pos2id = {}
        id2pos = {}

        # generate the dicts
        for sentence in total_list:

            for word in sentence:
                pos = word.attrib['pos']
                if pos not in pos2id.keys():
                    pos2id[pos] = len(pos2id)
                    id2pos[len(id2pos)] = pos

        save_structure(pos2id, self.params.pos2id)
        save_structure(id2pos, self.params.id2pos)

        return pos2id, id2pos

    def parse_test(self):
        """
        Parse the test file
        :return: list (sentences) of lists (sentence) of dictionaries (word)
            each word is a dict with hte keys (text, lemma, pos)
        """

        print("Parsing xml test file ...")

        new_sentences = load_struct(self.params.test_xml_res)
        if new_sentences != -1:
            return new_sentences

        with open(self.params.test_file) as file:
            sentences = file.readlines()

        new_sentences = []
        for sentence in sentences:

            new_sentence = []
            for word in sentence.split():
                attribs = word.split("|")

                new_word = {
                    'text': attribs[0],
                    'lemma': attribs[1],
                    'pos': attribs[2]
                }

                try:
                    new_word['id'] = attribs[3]
                except IndexError:
                    pass

                new_sentence.append(new_word)

            new_sentences.append(new_sentence)

        save_structure(new_sentences, self.params.test_xml_res)

        return new_sentences

    # -----------Other

    def generate_eval_train_sentence_list(self):

        eval_sentences = []
        train_sentences = []

        # get the senteces from eval and test
        for v in self.eval_xml.values():
            eval_sentences += v

        for v in self.train_xml.values():
            train_sentences += v

        return train_sentences, eval_sentences

    def test_predict_to_sentence(self, y_pred, y_true, meaning_seq):

        readable_sentence = []

        if not self.save_test_file:

            for idx in range(len(y_true)):

                if meaning_seq[idx]:
                    try:
                        readable_sentence.append(self.label2word[self.id2label[y_pred[idx]]])
                    except KeyError:
                        readable_sentence.append("unk")

                else:

                    readable_sentence.append(y_true[idx])
        else:
            for idx in range(len(y_true)):

                if meaning_seq[idx]:
                    try:
                        to_append=(y_true[idx],self.id2label[y_pred[idx]])
                    except KeyError:
                        to_append=(y_true[idx],self.most_common_senses[self.id2label[y_pred[idx]]])

                    except KeyError:
                        to_append=(y_true[idx],self.label2id[0])

                    finally:
                        readable_sentence.append(to_append)

        return readable_sentence

    # -----------BATCH
    def train_batch(self, use_meanings):

        # if there are no more keys inside the dict return
        if len(self.train_xml) == 0:
            return -1

        # get a random key from the train dict
        random_doc = random.choice(list(self.train_xml.keys()))

        # get a random sentence from the list
        sentence = self.train_xml[random_doc].pop(random.randint(0, len(self.train_xml[random_doc]) - 1))

        # remove empty document from dictionary
        if len(self.train_xml[random_doc]) == 0:
            self.train_xml.pop(random_doc)

        # update train len
        self.train_len = sum([len(elem) for elem in self.train_xml.values()])

        x_batch = []
        y_batch = []
        no_meaning_pad_batch = []
        meaning_batch_sent = []
        pos_batch = []

        for word in sentence:

            if use_meanings:
                meaning_batch_word = np.zeros(shape=self.label_num)

            try:
                look_for = word.text

                # look for the attribute 'id' inside the word (which is correlated to babelnet meaning id
                try:
                    # get the pointer to the right meaning id
                    key = word.attrib['id']
                    # get hte meaning id
                    key = self.train_key[key]
                    # append it to the y_batch list, casting from babelnet id to int
                    y_batch.append(self.label2id[key])
                    # update the meaning pad saying there is a meanign for this given word
                    no_meaning_pad_batch += [1]
                    if use_meanings:
                        # get the indices of the possible meanings for that word
                        indices = [self.label2id[elem] for elem in self.meaning_dict[word.attrib['lemma']]]
                        # append them to the meaning batch
                        meaning_batch_word[indices] = 1





                # menaing id not found, abbend unk and pad the no_meaning_pad_batch
                except KeyError:
                    y_batch.append(self.label2id['unk'])
                    no_meaning_pad_batch += [0]
                    meaning_batch_word = np.ones(shape=self.label_num)

                # get the corrresponding id for the word if saved
                embed = self.word2id[look_for]

            # if not saved use unk
            except KeyError:
                embed = self.word2id['unk']

            pos_batch.append(self.pos2id[word.attrib['pos']])
            x_batch.append(embed)

            if use_meanings:
                meaning_batch_sent.append(meaning_batch_word)

        return x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, pos_batch

    def train_batch_optimized(self, use_meanings):

        # if there are no more keys inside the dict return
        if len(self.train_xml) == 0:
            return -1

        # get a random key from the train dict
        random_doc = random.choice(list(self.train_xml.keys()))

        # get a random sentence from the list
        sentence = self.train_xml[random_doc].pop(random.randint(0, len(self.train_xml[random_doc]) - 1))

        # remove empty document from dictionary
        if len(self.train_xml[random_doc]) == 0:
            self.train_xml.pop(random_doc)

        # update train len
        self.train_len = sum([len(elem) for elem in self.train_xml.values()])

        x_batch = []
        y_batch = []
        no_meaning_pad_batch = []
        meaning_batch_sent = []
        pos_batch = []
        meaning_batch_word=[]

        for word in sentence:


            key = get_dict_value(word.attrib, 'id', 0)

            # menaing id not found, ebbend unk and pad the no_meaning_pad_batch
            if not key:
                y_batch.append(self.label2id['unk'])
                no_meaning_pad_batch += [0]

            # look for the attribute 'id' inside the word (which is correlated to babelnet meaning id
            else:
                # get hte meaning id
                key = self.train_key[key]
                # append it to the y_batch list, casting from babelnet id to int
                y_batch.append(self.label2id[key])
                # update the meaning pad saying there is a meanign for this given word
                no_meaning_pad_batch += [1]
                if use_meanings:
                    meaning_batch_word = np.zeros(shape=self.label_num,dtype=scipy.int8)
                    # get the indices of the possible meanings for that word
                    indices = [self.label2id[elem] for elem in self.meaning_dict[word.attrib['lemma']]]
                    # append them to the meaning batch
                    meaning_batch_word[indices] = 1


            look_for=get_dict_value(self.word2id,word.text,'unk')
            embed = look_for

            pos_batch.append(self.pos2id[word.attrib['pos']])
            x_batch.append(embed)

            if use_meanings:
                meaning_batch_sent.append(meaning_batch_word)

        return x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, pos_batch


    def eval_batch(self, use_meanings):

        # get a random key from the train dict
        random_doc = self.corpus_name

        if random_doc not in self.eval_xml.keys():
            return -1

        # get a random sentence from the listù
        try:
            sentence = self.eval_xml[random_doc].pop(random.randint(0, len(self.eval_xml[random_doc]) - 1))
        except KeyError:
            return -1

        # # revome empty document from dictionary
        # if len(self.eval_xml[random_doc]) == 0:
        #     self.eval_xml.pop(random_doc)
        #     self.corpus_name=list(self.eval_xml.keys())[0]

        # update eval len
        self.eval_len = sum([len(elem) for elem in self.eval_xml.values()])

        x_batch = []
        y_batch = []
        no_meaning_pad_batch = []
        meaning_batch_sent = []
        most_common_batch = []
        pos_batch = []

        for word in sentence:

            if use_meanings:
                meaning_batch_word = np.zeros(shape=self.label_num)
            try:
                look_for = word.text

                # look for the attribute 'id' inside the word (which is correlated to babelnet meaning id
                try:
                    # get the pointer to the right meaning id
                    key = word.attrib['id']
                    # get the meaning id
                    key = self.eval_key[key]
                    # append it to the y_batch list, casting from babelnet id to int
                    y_batch.append(self.label2id[key])
                    # update the meaning pad saying there is a meanign for this given word
                    no_meaning_pad_batch += [1]
                    # add most common meaning to the list
                    most_common_batch.append(self.label2id[self.most_common_senses[word.attrib['lemma']]])
                    if use_meanings:
                        # get the indices of the possible meanings for that word
                        indices = [self.label2id[elem] for elem in self.meaning_dict[word.attrib['lemma']]]
                        # append them to the meaning batch
                        meaning_batch_word[indices] = 1




                # menaing id not found, abbend unk and pad the no_meaning_pad_batch
                except KeyError:
                    y_batch.append(self.label2id['unk'])
                    no_meaning_pad_batch += [0]
                    most_common_batch.append(0)
                    meaning_batch_word = np.ones(shape=self.label_num)

                # get the corrresponding id for the word if saved
                embed = self.word2id[look_for]

            # if not saved use unk
            except KeyError:
                embed = self.word2id['unk']

            pos_batch.append(self.pos2id[word.attrib['pos']])
            x_batch.append(embed)

            if use_meanings:
                meaning_batch_sent.append(meaning_batch_word)

        return x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, most_common_batch, pos_batch

    def test_batch(self, use_meanings):

        # get a random sentence from the listù
        try:
            sentence = self.test_xml.pop(random.randint(0, self.test_len - 1))
        except KeyError:
            return -1

        # update eval len
        self.test_len = len(self.test_xml)

        x_batch = []
        y_batch = []
        no_meaning_pad_batch = []
        meaning_batch_sent = []
        most_common_batch = []
        pos_batch = []

        for word in sentence:

            look_for = word['text']

            # look for the attribute 'id' inside the word (which is correlated to babelnet meaning id
            if 'id' in word.keys():


                if self.save_test_file:
                    y_batch.append(word['id'])
                else:
                    y_batch.append(word['text'])
                # update the meaning pad saying there is a meaning for this given word
                no_meaning_pad_batch += [1]

                if word['lemma'] in self.most_common_senses.keys() and self.most_common_senses[
                    word['lemma']] in self.label2id.keys():
                    # add most common meaning to the list
                    most_common_batch.append(self.label2id[self.most_common_senses[word['lemma']]])
                else:
                    most_common_batch.append(0)

                if use_meanings and word['lemma'] in self.meaning_dict.keys():
                    meaning_batch_word = np.zeros(shape=self.label_num)
                    # get the indices of the possible meanings for that word
                    indices = []
                    for elem in self.meaning_dict[word['lemma']]:
                        if elem in self.label2id.keys():
                            indices.append(self.label2id[elem])
                    # append them to the meaning batch
                    meaning_batch_word[indices] = 1
                else:
                    meaning_batch_word = np.ones(shape=self.label_num)




            # menaing id not found, abbend unk and pad the no_meaning_pad_batch
            else:
                no_meaning_pad_batch += [0]
                most_common_batch.append(0)
                meaning_batch_word = np.ones(shape=self.label_num)
                y_batch.append(word['text'])

            if look_for in self.word2id.keys():
                # get the corrresponding id for the word if saved
                embed = self.word2id[look_for]

            # if not saved use unk
            else:
                embed = self.word2id['unk']

            pos_batch.append(self.pos2id[word['pos']])
            x_batch.append(embed)

            if use_meanings:
                meaning_batch_sent.append(meaning_batch_word)

        return x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, most_common_batch, pos_batch

    def generate_batch(self, batch_size, mode, use_meanings):
        """
        Generate a batch of sentences
        :param batch_size: (int) the number of sentences to pass
        :param mode: (string) the behavior of the function (train, eval, test)
        :param use_meanings: (bool) if true use the meaning matrix
        :returns:
            x_tensor: [batch_size,max_seq_len], the ids of various words
            y_tensor: [batch_size,max_seq_len], the correct labels ids
            no_meaning_pad_tensor: [batch_size,max_seq_len], zeros for words with no meaning, ones otherwise
            meaning_tensor: [batch_size,max_seq_len, label_num], ones associated with possible meaning ids, zeros otherwise
            most_common_tensor:  [batch_size,max_seq_len], MFS for every words
            pos_tensor: [batch_size,max_seq_len]: ids dor pos
        """

        if mode == "train":
            if len(self.train_xml) < batch_size:
                return -1
        elif mode == "eval":

            # if there are no more data in eval return
            if len(self.eval_xml) == 0:
                return -1
            # if there are not enough data in the specific corpus of the eval set
            elif len(self.eval_xml[self.corpus_name]) < batch_size:
                # delete the current corpus
                self.eval_xml.pop(self.corpus_name)
                # try to update the name
                try:
                    self.corpus_name = list(self.eval_xml.keys())[0]
                finally:
                    return -1

        elif mode == "test":
            # if there are no more data in eval return
            if self.test_len < batch_size:
                return -1


        else:
            raise Exception(f"Mode '{mode}' not valid")

        y_tensor = []
        x_tensor = []
        no_meaning_pad_tensor = []
        meaning_tensor = []
        most_common_tensor = []
        pos_tensor = []
        seq_len=[]

        for idx in range(batch_size):

            if mode == "train":

                out = self.train_batch(use_meanings)

                # if the output is -1
                if isinstance(out, int):
                    # if there is some data to look for return it
                    if len(x_tensor) > 0:
                        break
                    else:
                        # return -1
                        return out

                x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, pos_batch = out
                most_common_batch = []

            elif mode == "eval":

                out = self.eval_batch(use_meanings)

                # if the output is -1
                if isinstance(out, int):
                    # if there is some data to look for return it
                    if len(x_tensor) > 0:
                        break
                    else:
                        return out

                x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, most_common_batch, pos_batch = out


            elif mode == "test":
                out = self.test_batch(use_meanings)

                # if the output is -1
                if isinstance(out, int):
                    # if there is some data to look for return it
                    if len(x_tensor) > 0:
                        break
                    else:
                        return out

                x_batch, y_batch, no_meaning_pad_batch, meaning_batch_sent, most_common_batch, pos_batch = out



            else:
                raise Exception(f"Mode '{mode}' not valid")

            y_tensor.append(y_batch)
            x_tensor.append(x_batch)
            seq_len.append(len(y_batch))
            no_meaning_pad_tensor.append(no_meaning_pad_batch)
            most_common_tensor.append(most_common_batch)
            pos_tensor.append(pos_batch)
            if use_meanings:
                meaning_tensor.append(meaning_batch_sent)

        max_seq_len = max(seq_len)

        for idx in range(len(x_tensor)):

            if len(x_tensor[idx]) < max_seq_len:
                x_tensor[idx] += [0] * (max_seq_len - len(x_tensor[idx]))
                y_tensor[idx] += [0] * (max_seq_len - len(y_tensor[idx]))
                no_meaning_pad_tensor[idx] += [0] * (max_seq_len - len(no_meaning_pad_tensor[idx]))
                most_common_tensor[idx] += [0] * (max_seq_len - len(most_common_tensor[idx]))
                pos_tensor[idx] += [0] * (max_seq_len - len(pos_tensor[idx]))

                if use_meanings:
                    for i in range(max_seq_len - len(meaning_tensor[idx])):
                        meaning_tensor[idx].append(np.zeros(self.label_num))

        if not use_meanings:
            meaning_tensor = np.ones(shape=[batch_size, max_seq_len, self.label_num])

        return np.asarray(x_tensor), np.asarray(y_tensor), seq_len, np.asarray(no_meaning_pad_tensor), np.asarray(
            meaning_tensor), np.asarray(most_common_tensor), np.asarray(pos_tensor)
