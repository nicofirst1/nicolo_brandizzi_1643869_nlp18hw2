import tensorflow as tf
import pprint

from termcolor import colored


class ModelParam():
    """Class to hold the parameters for the model

        Atrributes:

                Rnn Cells parameters:
                    num_unit (int): The number of units in the GRU cell.
                    activation (tf.function):  Activation function for the cell.
                    forget_bias (float): The bias added to forget gates
                    use_gru (bool): If true use GRU rnn cell, else use LSTM cell
                    stack_layers (bool): usa lstm stack instad of single one
                    lstm_peephole (bool): use the peephole in lstm
                    drop_out (bool): use dropout wrapper
                    n_layer (int): number of layer per stack is stack_layers is enabled
                    keep_prob (float): the probability to keep a neuron input/output if dropout is enabled

                Inputs:
                    batch_size (int): the number of batch (i.e. sentences) in input
                    word_embedding_size (int): the dimension of the embeddings for words
                    pos_embedding_size (int): the dimension of the embeddings for pos
                    epochs (int): number of epochs

                Masks:
                    use_meanings_mask (bool): whenever to use the meaning 3d mask or not
                    use_eval (bool): determine if you want to use the evaluation set too when getting the most common senses
                    common_meaning_thrs (float): the percentage of confidence for a softamx prediction,
                        every prediction with a score lower than this number is discarded and most common sense is used instead

                Loss:
                    use_pos (bool): use pos tagging information in the bidirectional rnn
                    use_softamx_loss (bool): add the number of non-softmax parameter used in the prediction and minimize
                                            the negative
                    softmax_prediction_weight (float): percentage of how much the above loss is to be considered
                    use_f1_loss (bool): add f1 to the loss
                    f1_loss_weight(float): how much of the f1 loss to be considered

                Optimizer:
                    learning_rate (float): learning rate for the optimizer
                    decay_rate (float): the decay value for the optimizer
                    decay_step (int): the number of step before applying the dacy

                Other:
                    print_perc (int): the percentual at wich you want to print the infos (loss, accuracy) for each epoch
                    debug (bool): display debug information if true
                """

    def __init__(self):

        # -------RNN CELLS
        self.hidden_size = 512
        self.activation =  tf.nn.tanh
        self.forget_bias = 1.0
        self.use_gru = False
        self.stack_layers = False
        self.n_layer = 2
        self.lstm_peephole = False #futile
        self.drop_out = False # no good
        self.keep_prob = 0.85

        # --------INPUTS
        self.batch_size = 32
        self.epochs = 5
        self.word_embedding_size = 300
        self.pos_embedding_size = 300

        self.use_attention=False
        self.max_seq_len=128

        # --------MASK
        self.use_meanings_mask = True
        self.common_meaning_thrs = 0.6
        self.use_eval = True

        # --------LOSS
        self.use_pos = True
        self.use_softamx_loss = False
        self.softmax_prediction_weight = 0.6
        self.use_f1_loss = False
        self.f1_loss_weight = 10
        self.use_incorrect_common = False #futile
        self.incorrect_common_weight = 0.4

        # ---------OPTIMIZER
        self.learning_rate = 0.001
        self.use_decay = True
        self.decay_rate = 0.000001
        self.decay_step = 500

        # ----------OTHER
        self.print_perc = 10
        self.debug = False

    def print(self):
        pp = pprint.PrettyPrinter(indent=4)
        to_print = pp.pformat(self.__dict__)
        print(colored(to_print, color="cyan"))

    def write_params(self, log_dir):
        pp = pprint.PrettyPrinter(indent=4)
        to_write = pp.pformat(self.__dict__)

        with open(log_dir + "params", "w+") as file:
            file.writelines(to_write)


class DataParam():
    """Class to hold the parameters for the model

        Atrributes:

                Directories and Files:
                    RESOURCES_DIR (str, const): the direcotry in which the resources are stored
                    embed_dict_file (str, const): path to the word->embed dict
                    class_dict_file (str, const): path to the meaning_id->int dict

                    DATA_DIR (str,const): path to the dir in which the datas are stored
                    TEST_DIR (str,const): path to the dir in which the test data is stored
                    TRAIN_DIR (str,const): path to the dir in which the train data is stored
                    babelfy_v_file (str,const): path to the babelfy semantic embeddings file
                    babelfy_vf_file (str,const): path to the filtered babelfy semantic embeddings file

                """

    def __init__(self):
        # directories and files
        self.RESOURCES_DIR = "Resources/"
        self.embed_dict_file = self.RESOURCES_DIR + "embed_dict.pkl"
        self.class_dict_file = self.RESOURCES_DIR + "class_dict.pkl"
        self.meaning_dict_file = self.RESOURCES_DIR + "meaning_dict.pkl"
        self.most_common_senses = self.RESOURCES_DIR + "common_sense.pkl"

        self.train_xml_res = self.RESOURCES_DIR + "train_xml.pkl"
        self.test_xml_res = self.RESOURCES_DIR + "test_xml.pkl"
        self.eval_xml_res = self.RESOURCES_DIR + "eval_xml.pkl"

        self.train_key_res = self.RESOURCES_DIR + "train_key.pkl"
        self.eval_key_res = self.RESOURCES_DIR + "eval_key.pkl"

        self.word2id = self.RESOURCES_DIR + "word2id.pkl"
        self.id2word = self.RESOURCES_DIR + "id2wrod.pkl"

        self.label2id = self.RESOURCES_DIR + "label2id.pkl"
        self.id2label = self.RESOURCES_DIR + "id2label.pkl"

        self.word2label = self.RESOURCES_DIR + "word2label.pkl"
        self.label2word = self.RESOURCES_DIR + "label2word.pkl"

        self.pos2id = self.RESOURCES_DIR + "pos2id.pkl"
        self.id2pos = self.RESOURCES_DIR + "id2pos.pkl"

        self.DATA_DIR = "../DataSets/HW2/"
        self.EVAL_DIR = self.DATA_DIR + "Eval/"
        self.TRAIN_DIR = self.DATA_DIR + "Train/"
        self.babelfy_v_file = self.DATA_DIR + "babelfy_vectors"
        self.babelfy_vf_file = self.DATA_DIR + "babelfy_vectors_filtered"
        self.train_xml = self.TRAIN_DIR + "semcor.data.xml"
        self.train_key = self.TRAIN_DIR + "semcor.gold.key.bnids.txt"
        self.eval_xml = self.EVAL_DIR + "ALL.data.xml"
        self.eval_key = self.EVAL_DIR + "ALL.gold.key.bnids.txt"

        self.CURRENT_MODEL = "Current_Model/"
        self.LOG_DIR = self.CURRENT_MODEL + "Logs/"
        self.INFOS_DIR = self.CURRENT_MODEL + "Infos/"
        self.MODEL_DIR = self.CURRENT_MODEL + "Model/"
        self.MODEL_NAME = "model"

        self.f1_scores_file = self.LOG_DIR + "f1_scores"

        self.test_file = self.DATA_DIR + "test_file"
