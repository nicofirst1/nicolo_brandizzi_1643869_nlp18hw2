import time

import numpy as np
import tensorflow as tf
from termcolor import colored

from utils import perc_print, f1_score, analyze_incorrect_meanings, apply_meaning_tensor, \
    apply_most_common_senses, normalize_tensors
from utils import time_print


class Model:
    """ The class that hold the rnn model

        Syntax:
            instace_name (object type): [dimensions], description
            Both the (object type) and the dimensions are optional and not always reported.
            Almost all the dimensions are located in the parameter class with the same name as shown in the [],
            some of the dimension not illustrated in the param class are:
                padded_seq_len: the model performs a dynamic padding for each batch of sentence in which every sentence is
                            padded to the maximum length of the sentences in the current batch


        Attributes:
            params (class) : the class containig the parameters for the model
            data_batch (class): The DataBatch class which handles all the data computation

            Other:
                graph: the default graph for the project
                init : the tf initializer (tf.global_variables_initializer())
                proto_config: the proto configuration for the tensorflow session
                merged : the merged object used to log variables in tensorboard
                saver : the saver object to save and restore a model

            Place Holders:
                y: [batch_size, padded_seq_len], the correct labels for each meaning of a word
                seq_len: [batch_size], an int representing the original (not padded) length of each sentence
                no_meaning_seq: [batch_size, padded_seq_len], a boolean matrix in which zeros are for the words which have no
                                meaning, while ones are for the ones with meanings
                word_ids: [batch_size, padded_seq_len], the model input (X) which contains an id for each word
                meaning_tensor: [batch_size, padded_seq_len, label_num], a 3d tensor in which the possible meanings for a word
                                are ones while the others are zeros
            Variables:
                weights: [2 * hidden_size, label_num], the model weights
                biases: [label_num], the model biases
                embed_matrix: [vocabulary_size, embedding_size], the matrix embeddings
                embed_lookup: [vocabulary_size, embedding_size], used to convert word ids to embeddings
                sequence_mask : [batch_size], an array which contains the original lenght of the sentences
                no_meaning_mask: [batch_size, padded_seq_len], same asno_meaning_seq
                scores : A variable used for the accuracy/evaluation task, it is not a matrix since it undergoes some mask
                        passages to filter out various elements

            RNN Layer:
                fw_word_cell: can bee either a LSTM or a GRU cell, it is used in the forward passage
                bk_word_cell: can bee either a LSTM or a GRU cell, it is used in the backward passage
                logits: [batch_size, seq_len,2*hidden_size], the result of the stacked bidirectoinal rnn, with weights multiplication
                        and bias addiction

            Loss and Optimizer:
                loss: a matrix where some mask operation have been done, it is used as loss (duh!)
                opt: stands for Optimizer, it is used to minimize the loss
                train_op: the actual minimization of the loss function
                accuracy: a mean that estimates the accuracy on the train dataset

            """

    def __init__(self, parameter_class, data_batch, debug):
        """
        :param data_batch: The DataBatch class which handles all the data computation
        :param parameter_class: the class for the model parameters
        :param debug: (bool), decide to print debug infos or not
        """

        self.data_batch = data_batch
        self.params = parameter_class
        self.debug = debug

        # Other
        self.graph = tf.get_default_graph()
        self.init = None
        self.proto_config = None
        self.merged = None
        self.saver = None
        self.f1_scores_baseline_values = [0.656, 0.66, 0.544, 0.638, 0.671]

        # tensorflow placeholder
        self.y_word = None
        self.seq_len = None
        self.no_meaning_seq = None
        self.word_ids = None
        self.meaning_tensor = None

        # tensorflow variables
        self.weights = None
        self.biases = None
        self.word_embed_matrix = None
        self.word_embed_lookup = None
        self.sequence_mask = None
        self.no_meaning_mask = None
        self.step = None
        self.predictions = None

        # rnn layer
        self.fw_word_cell = None
        self.bk_word_cell = None
        self.fw_pos_cell = None
        self.bk_pos_cell = None
        self.logits = None

        # tensorflow loss and optimizer
        self.loss = None
        self.opt = None
        self.train_op = None
        self.accuracy = None

    def init_graph(self):

        print("Init graph...")
        # append everything to the gpu
        with tf.device("/gpu:0"):

            # reset the default graph
            tf.reset_default_graph()

            # proto config
            self.proto_config = tf.ConfigProto(log_device_placement=False, allow_soft_placement=True)
            self.proto_config.gpu_options.allow_growth = True

            # using the graph as default to declare all the tf variables
            with self.graph.as_default():

                with tf.name_scope("input_data"):

                    self.word_ids = tf.placeholder(dtype=tf.int32, shape=[None, None], name="word_ids")
                    self.pos_ids = tf.placeholder(dtype=tf.int32, shape=[None, None], name="pos_ids")

                    self.y_word = tf.placeholder(dtype=tf.int32, shape=[None, None], name="y_word")
                    self.y_pos = tf.placeholder(dtype=tf.int32, shape=[None, None], name="y_pos")

                    self.no_meaning_seq = tf.placeholder(dtype=tf.int64, shape=[None, None], name="no_meaning_seq_len")
                    self.meaning_tensor = tf.placeholder(dtype=tf.int64, shape=[None, None, None],
                                                         name="possible_meaning_tensor")
                    #self.seq_len= tf.placeholder(dtype=tf.int32, shape=[None], name="seq_len")

                with tf.name_scope("f1_scores"):

                    self.f1_scores_tensors = [
                        tf.placeholder(dtype=tf.float32, shape=(), name="f1_score_senseval2"),
                        tf.placeholder(dtype=tf.float32, shape=(), name="f1_score_senseval3"),
                        tf.placeholder(dtype=tf.float32, shape=(), name="f1_score_semeval2007"),
                        tf.placeholder(dtype=tf.float32, shape=(), name="f1_score_semeval2013"),
                        tf.placeholder(dtype=tf.float32, shape=(), name="f1_score_semeval2015"),
                    ]

                    self.f1_scores_values = [0, 0, 0, 0, 0]

                    self.mean_f1_score = tf.placeholder(dtype=tf.float32, shape=(), name="mean_f1_score")

                with tf.name_scope("Infos"):
                    self.softmax_usage_tensor = tf.placeholder(dtype=tf.float32, shape=(), name="softmax_usage")
                    self.most_common_usage_tensor = tf.placeholder(dtype=tf.float32, shape=(), name="most_common_usage")
                    self.incorrect_common_tensor = tf.placeholder(dtype=tf.float32, shape=(), name="incorrect_common")
                    self.softmax_usage_val = 0.0
                    self.most_common_usage_val = 0.0
                    self.incorrect_common_val = 0.0
                    self.debug_tensor = tf.placeholder(dtype=tf.float32, shape=(), name="debug_tensor")

                with tf.name_scope("words_varaibles"):

                    self.weights = tf.Variable(
                        tf.truncated_normal(shape=[2 * self.params.hidden_size, self.data_batch.label_num]),
                        name="weights")
                    self.biases = tf.Variable(tf.zeros(shape=[self.data_batch.label_num]), name="biases")


                    self.word_embed_matrix = tf.Variable(
                        tf.random_uniform([self.data_batch.vocabulary_size, self.params.word_embedding_size], -1.0,
                                          1.0),
                        name="word_embeddings")
                    self.word_embed_lookup = tf.nn.embedding_lookup(self.word_embed_matrix, self.word_ids,
                                                                    name="word_lookup")

                    self.step = tf.Variable(0, trainable=False, name="step")

                with tf.name_scope("pos_variables"):

                    self.pos_embed_matrix = tf.Variable(
                        tf.random_uniform([self.data_batch.pos_size, self.params.pos_embedding_size], -1.0,
                                          1.0),
                        name="pos_embeddings")
                    self.pos_embed_lookup = tf.nn.embedding_lookup(self.pos_embed_matrix, self.pos_ids,
                                                                   name="pos_lookup")

                with tf.name_scope("Rnn_variables"):

                    # decide whenever to use gru/stack/lstm cells
                    if self.params.use_gru:
                        self.fw_word_cell, self.bk_word_cell = self.gru_cell()
                    elif self.params.stack_layers:
                        self.fw_word_cell, self.bk_word_cell = self.stack_rnn_cell()
                    else:
                        self.fw_word_cell, self.bk_word_cell, self.fw_pos_cell, self.bk_pos_cell = self.lstm_cell()

                    self.no_meaning_mask = tf.cast(tf.sequence_mask(self.no_meaning_seq), tf.float32)
                    self.meaning_tensor_mask = tf.cast(tf.sequence_mask(self.meaning_tensor), tf.float32)

                    outputs = self.bidirectional_rnn()  # [batch_size, seq_len, label_num]

                    if self.params.use_attention:
                        outputs=self.attention_layer(outputs)

                    self.logits=self.weights_and_biases(outputs)

                    logits = self.logits

                with tf.name_scope("loss_optimizer"):

                    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
                        logits=logits, labels=self.y_word)  # [batch_size, seq_len]

                    loss = tf.boolean_mask(loss, tf.squeeze(self.no_meaning_mask, axis=-1))

                    self.loss = tf.reduce_mean(loss, name="masked_loss")

                    if self.params.use_softamx_loss:
                        # self.loss += self.most_common_usage_tensor * self.params.softmax_prediction_weight

                        self.loss += self.most_common_usage_tensor * self.params.softmax_prediction_weight

                    if self.params.use_f1_loss:
                        # self.loss -= self.mean_f1_score * self.params.f1_loss_weight

                        self.loss -= self.mean_f1_score * self.params.f1_loss_weight

                    if self.params.use_incorrect_common:
                        # self.loss += self.incorrect_common_tensor
                        self.loss += self.loss*self.incorrect_common_tensor * self.params.incorrect_common_weight

                    if self.params.use_decay:
                        rate = tf.train.exponential_decay(learning_rate=self.params.learning_rate,
                                                          global_step=self.step, decay_steps=self.params.decay_step,
                                                          decay_rate=self.params.decay_rate)
                        self.opt = tf.train.AdamOptimizer(rate, name="opt")
                    else:
                        self.opt = tf.train.AdamOptimizer(self.params.learning_rate, name="opt")

                    self.train_op = self.opt.minimize(self.loss, name="train_opt")

                with tf.name_scope("eval"):
                    scores = tf.nn.softmax(logits)

                    scores = scores * tf.cast(self.meaning_tensor, tf.float32)

                    self.scores = scores
                    scores = tf.argmax(scores, axis=-1)

                    scores = tf.boolean_mask(scores, tf.squeeze(self.no_meaning_mask, axis=-1))
                    self.predictions = scores

                with tf.name_scope("accuracy"):

                    y = tf.boolean_mask(self.y_word, tf.squeeze(self.no_meaning_mask, axis=-1))

                    correct_prediction = tf.equal(self.predictions, tf.cast(y, tf.int64))
                    self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, dtype=tf.float32), name="accuracy")

                with tf.name_scope("summaries"):

                    # define initializator and summary merged
                    tf.summary.scalar("train_loss", self.loss)
                    tf.summary.scalar("train_accuracy", self.accuracy)
                    tf.summary.scalar("softamx_usage", self.softmax_usage_tensor)
                    tf.summary.scalar("incorrect common", self.incorrect_common_tensor)

                    for elem in self.f1_scores_tensors:
                        tf.summary.scalar(f"{elem.name}", elem)

                    self.merged = tf.summary.merge_all()

                self.init = tf.global_variables_initializer()
                self.saver = tf.train.Saver()

    def gru_cell(self):
        """
        Gru cells (fw,bk) initializer
        :return (tuple): fw,bk cells
        """

        print("Using Gru cell")

        fw = tf.nn.rnn_cell.GRUCell(self.params.hidden_size, self.params.activation, name="fw_cell")
        bk = tf.nn.rnn_cell.GRUCell(self.params.hidden_size, self.params.activation, name="bk_cell")

        return fw, bk

    def attention_layer(self,outputs):

        with tf.name_scope("attention"):
            att_output, att =AttentionWithContext(outputs.get_shape().as_list())(outputs,tf.sequence_mask(self.seq_len))

            att_output=tf.tile(att_output,[1,self.params.max_seq_len])
            att_output=tf.reshape(att_output,(-1,self.params.max_seq_len,self.params.hidden_size*2))
            att_output=tf.concat([att_output,outputs],axis=-1)
            return att_output


    def lstm_cell(self):
        """
        LSTM cells (fw,bk) initializer
        :return (tuple): fw,bk cells
        """

        print("Using LSTM cell")

        fw_word = tf.nn.rnn_cell.LSTMCell(self.params.hidden_size,
                                          activation=self.params.activation,
                                          name="fw_word_cell",
                                          forget_bias=self.params.forget_bias,
                                          use_peepholes=self.params.lstm_peephole)

        bk_word = tf.nn.rnn_cell.LSTMCell(self.params.hidden_size,
                                          activation=self.params.activation,
                                          name="bk_word_cell",
                                          forget_bias=self.params.forget_bias,
                                          use_peepholes=self.params.lstm_peephole)

        fw_pos = tf.nn.rnn_cell.LSTMCell(self.params.hidden_size,
                                         activation=self.params.activation,
                                         name="fw_pos_cell",
                                         forget_bias=self.params.forget_bias,
                                         use_peepholes=self.params.lstm_peephole)

        bk_pos = tf.nn.rnn_cell.LSTMCell(self.params.hidden_size,
                                         activation=self.params.activation,
                                         name="bk_pos_cell",
                                         forget_bias=self.params.forget_bias,
                                         use_peepholes=self.params.lstm_peephole)
        if self.params.drop_out:
            fw_word = tf.contrib.rnn.DropoutWrapper(fw_word, input_keep_prob=self.params.keep_prob,
                                                    output_keep_prob=self.params.keep_prob)
            bk_word = tf.contrib.rnn.DropoutWrapper(bk_word, input_keep_prob=self.params.keep_prob,
                                                    output_keep_prob=self.params.keep_prob)
            fw_pos = tf.contrib.rnn.DropoutWrapper(fw_pos, input_keep_prob=self.params.keep_prob,
                                                   output_keep_prob=self.params.keep_prob)
            bk_pos = tf.contrib.rnn.DropoutWrapper(bk_pos, input_keep_prob=self.params.keep_prob,
                                                   output_keep_prob=self.params.keep_prob)

        return fw_word, bk_word, fw_pos, bk_pos

    def stack_rnn_cell(self):
        """
        Stack of LSTM cell
        :return:
        """

        print("Using stack lstm cells")

        layers_fw = [tf.nn.rnn_cell.LSTMCell(self.params.hidden_size,
                                             activation=self.params.activation,
                                             name="bk_cell",
                                             forget_bias=self.params.forget_bias,
                                             use_peepholes=self.params.lstm_peephole)

                     for _ in range(self.params.n_layer)]
        layers_bk = [tf.nn.rnn_cell.LSTMCell(self.params.hidden_size,
                                             activation=self.params.activation,
                                             name="bk_cell",
                                             forget_bias=self.params.forget_bias,
                                             use_peepholes=self.params.lstm_peephole)]

        if self.params.drop_out:
            layers_fw = [tf.contrib.rnn.DropoutWrapper(elem, input_keep_prob=self.params.keep_prob,
                                                       output_keep_prob=self.params.keep_prob) for elem in layers_fw]
            layers_bk = [tf.contrib.rnn.DropoutWrapper(elem, input_keep_prob=self.params.keep_prob,
                                                       output_keep_prob=self.params.keep_prob) for elem in layers_bk]

        fw_stack = tf.contrib.rnn.MultiRNNCell(layers_fw)
        bk_stack = tf.contrib.rnn.MultiRNNCell(layers_bk)

        return fw_stack, bk_stack

    def bidirectional_rnn(self):
        """
            Create the bidirectional dynamic rnn
            where we feed two cells of size [hidden_size]
            The outputs are two of [batch_size, seq_len, hidden_size], which concatenated on the last (-1)
            axis becomes [batch_size, seq_len,2*hidden_size]
            :return: tensor of size [batch_size, seq_len, label_num]
        """
        if self.params.use_pos:
            (output_fw, output_bk), _ = tf.nn.bidirectional_dynamic_rnn(self.fw_word_cell, self.bk_word_cell,
                                                                        inputs=tf.concat([self.word_embed_lookup,
                                                                                          self.pos_embed_lookup],
                                                                                         axis=-1),
                                                                        sequence_length=self.seq_len,
                                                                        dtype=tf.float32, )

        else:
            (output_fw, output_bk), _ = tf.nn.bidirectional_dynamic_rnn(self.fw_word_cell, self.bk_word_cell,
                                                                        inputs=self.word_embed_lookup,
                                                                        sequence_length=self.seq_len,
                                                                        dtype=tf.float32,
                                                                        )
            # concatenate outputs
        outputs = tf.concat([output_fw, output_bk], axis=-1)
        return outputs

    def weights_and_biases(self,outputs):
        # save timestep dim for later use
        time_step = tf.shape(outputs)[1]
        # reshape for multiplication
        outputs = tf.reshape(outputs, [-1, 2 * self.params.hidden_size])
        # execute multiplication
        mult = tf.matmul(outputs, self.weights) + self.biases
        # reshape back and return
        return tf.reshape(mult, [-1, time_step, self.data_batch.label_num])

    def train(self):

        average_loss = 0.0
        average_time = 0.0
        idx = 1

        log_perc = 0.01
        eval_perc = 0.15

        with tf.Session(graph=self.graph, config=self.proto_config) as sess:
            print("Trainig...")

            writer = tf.summary.FileWriter(self.data_batch.params.LOG_DIR, graph=sess.graph)
            self.init.run()

            self.saver = tf.train.Saver(tf.global_variables())

            self.pythonic_evaluate(False, custom_sess=sess)

            # for every epoch
            for epoch in range(self.params.epochs):

                # set the train data
                self.data_batch.set_train()
                # get the initial length
                original_len = self.data_batch.train_len

                print(f"Total number of step per epoch: {original_len}\n"
                      f"Steps for logging: {np.ceil(log_perc*original_len)}\n"
                      f"Steps for evaluation: {np.ceil(eval_perc*original_len)}\n"
                      f"Average time per step :{average_time/idx}\n"
                      f"ETA per epoch : {time_print(average_time/idx*original_len/self.params.batch_size)}\n"
                      f"ETA per whole execution : {time_print(average_time/idx*original_len*(self.params.epochs-epoch)//self.params.batch_size)}")

                # repeat until there are some sentences left
                while True:

                    start = time.time()

                    # get the batch
                    out = self.data_batch.generate_batch(self.params.batch_size, "train", self.params.use_meanings_mask)

                    # check if there is data still avaiable
                    if isinstance(out, int):
                        break

                    # unpack the data
                    x_batch, y_batch, seq_len, no_meaning_seq, meaning_tensor, _, pos_tensor = out

                    # tesor_analizer(meaning_tensor)

                    # initialize feed_dict
                    feed_dict = {self.word_ids: x_batch, self.y_word: y_batch,
                                 self.no_meaning_seq: no_meaning_seq,
                                 self.meaning_tensor: meaning_tensor,
                                 self.pos_ids: pos_tensor,
                                 self.seq_len:seq_len}

                    # update dict with f1 scores
                    feed_dict.update({k: v for k, v in zip(self.f1_scores_tensors, self.f1_scores_values)})
                    feed_dict.update({self.mean_f1_score: sum(self.f1_scores_values)})

                    # update dict with softmax_vs_most_common value
                    feed_dict.update({self.softmax_usage_tensor: self.softmax_usage_val})
                    feed_dict.update({self.most_common_usage_tensor: self.most_common_usage_val})
                    feed_dict.update({self.incorrect_common_tensor: self.incorrect_common_val})

                    # Perform the run
                    _, loss, accuracy, summary = sess.run(
                        [self.train_op, self.loss, self.accuracy, self.merged],
                        feed_dict=feed_dict,
                    )
                    average_loss += loss


                    # save scalars
                    if idx % np.ceil(original_len * log_perc / self.params.batch_size) == 0:
                        writer.add_summary(summary, idx)
                        # print("scalar saved in summary")

                    # perform evaluation
                    if idx % np.ceil(original_len * eval_perc / self.params.batch_size) == 0:
                        print(f"\nloss: {average_loss/idx}\n"
                              f"accuracy:{accuracy}\n"
                              f"average time per step :{average_time/idx}\n"
                              f"ETA per current epoch : {time_print(average_time/idx*self.data_batch.train_len/self.params.batch_size)}")
                        self.pythonic_evaluate(False, custom_sess=sess)

                    # print out the current epoch status
                    perc_print(self.data_batch.train_len, original_len, msg=f"Epoch {epoch}/{self.params.epochs} at ",
                               inverted=True)

                    # get the time for oen step
                    average_time += (time.time() - start)
                    idx += 1

                # save the session at each epoch
                self.saver.save(sess, self.data_batch.params.MODEL_DIR + "model", global_step=epoch)

    def restore_and_train(self):

        average_loss = 0.0
        average_time = 0.0
        idx = 1

        log_perc = 0.05
        eval_perc = 0.25

        with tf.Session(graph=self.graph, config=self.proto_config) as sess:
            print("Trainig...")

            writer = tf.summary.FileWriter(self.data_batch.params.LOG_DIR, graph=sess.graph)

            self.saver.restore(sess, tf.train.latest_checkpoint("./Model"))

            # for every epoch
            for epoch in range(self.params.epochs):

                # set the train data
                self.data_batch.set_train()
                # get the initial length
                original_len = self.data_batch.train_len

                print(f"Total number of step per epoch: {original_len}\n"
                      f"Steps for logging: {np.ceil(log_perc*original_len)}\n"
                      f"Steps for evaluation: {np.ceil(eval_perc*original_len)}\n"
                      f"Average time per step :{average_time/idx}\n"
                      f"ETA per epoch : {time_print(average_time/idx*original_len/self.params.batch_size)}\n"
                      f"ETA per whole execution : {time_print(average_time/idx*original_len*(self.params.epochs-epoch)//self.params.batch_size)}")
                # repeat until there are some sentences left
                # repeat until there are some sentences left
                while True:

                    start = time.time()

                    # get the batch
                    out = self.data_batch.generate_batch(self.params.batch_size, "train", self.params.use_meanings_mask)

                    # check if there is data still avaiable
                    if isinstance(out, int):
                        break

                    # unpack the data
                    x_batch, y_batch, seq_len, no_meaning_seq, meaning_tensor, _, pos_tensor = out

                    # tesor_analizer(meaning_tensor)

                    # initialize feed_dict
                    feed_dict = {self.word_ids: x_batch, self.y_word: y_batch,
                                 self.seq_len: seq_len,
                                 self.no_meaning_seq: no_meaning_seq,
                                 self.meaning_tensor: meaning_tensor,
                                 self.pos_ids: pos_tensor,}

                    # update dict with f1 scores
                    feed_dict.update({k: v for k, v in zip(self.f1_scores_tensors, self.f1_scores_values)})
                    feed_dict.update({self.mean_f1_score: sum(self.f1_scores_values)})

                    # update dict with softmax_vs_most_common value
                    feed_dict.update({self.softmax_usage_tensor: self.softmax_usage_val})
                    feed_dict.update({self.most_common_usage_tensor: self.most_common_usage_val})
                    feed_dict.update({self.incorrect_common_tensor: self.incorrect_common_val})

                    # Perform the run
                    _, loss, accuracy, summary = sess.run(
                        [self.train_op, self.loss, self.accuracy, self.merged],
                        feed_dict=feed_dict,
                    )
                    average_loss += loss

                    # save scalars
                    if idx % np.ceil(original_len * log_perc / self.params.batch_size) == 0:
                        writer.add_summary(summary, idx)
                        # print("scalar saved in summary")

                    # perform evaluation
                    if idx % np.ceil(original_len * eval_perc / self.params.batch_size) == 0:
                        print(f"\nloss: {average_loss/idx}\n"
                              f"accuracy:{accuracy}\n"
                              f"average time per step :{average_time/idx}\n"
                              f"ETA per current epoch : {time_print(average_time/idx*self.data_batch.train_len/self.params.batch_size)}")
                        self.pythonic_evaluate(False, custom_sess=sess)

                    # print out the current epoch status
                    perc_print(self.data_batch.train_len, original_len, msg=f"Epoch {epoch}/{self.params.epochs} at ",
                               inverted=True)

                    # get the time for oen step
                    average_time += (time.time() - start)
                    idx += 1

                # save the session at each epoch
                self.saver.save(sess, self.data_batch.params.MODEL_DIR + "model", global_step=epoch)

    def numpy_evaluate(self, restore, custom_sess=None, graph=None):

        idx = 0
        # reset the eval data if empty
        if len(self.data_batch.eval_xml) == 0:
            self.data_batch.set_eval()

        with tf.Session(graph=graph, config=self.proto_config) as sess:

            if custom_sess:
                sess = custom_sess

            if restore:
                saver = tf.train.import_meta_graph(self.data_batch.params.MODEL_DIR + "model.meta")
                saver.restore(sess, self.data_batch.params.MODEL_DIR + "checkpoint")
            else:
                self.init.run()

            print("Evaluation...")

            # generate original sentence matrix for feeding
            for i in range(len(self.data_batch.eval_xml.keys())):
                # self.data_eval.generate_batch(corpus_names[0], self.params.batch_size)
                y_pred = []
                y_true = []
                current_corpus = self.data_batch.corpus_name

                # repeat until there are some sentences left
                while True:
                    # get the batch
                    batch_info = self.data_batch.generate_batch(self.params.batch_size, "eval",
                                                                self.params.use_meanings_mask)

                    # if the current corpus has no more sentence break
                    if isinstance(batch_info, int):
                        break

                    # unpack the infos
                    x_batch, y_batch, seq_len, no_meaning_seq, meaning_tensor = batch_info

                    # initialize feed_dict
                    feed_dict = {self.word_ids: x_batch,
                                 self.seq_len: seq_len,
                                 self.no_meaning_seq: no_meaning_seq,
                                 self.meaning_tensor: meaning_tensor}

                    feed_dict.update({k: v for k in self.f1_scores_tensors for v in self.f1_scores_values})

                    # Perform the run
                    scores = sess.run(
                        [self.predictions],
                        feed_dict=feed_dict, )

                    idx += 1
                    scores = list(scores[0])
                    for j in range(len(seq_len)):
                        scores[j] = scores[j][:seq_len[j]]

                    y_batch = list(y_batch)
                    for j in range(len(seq_len)):
                        y_batch[j] = y_batch[j][:seq_len[j]]

                    y_batch = np.asarray(y_batch)
                    scores = np.asarray(scores)

                    y_true.append(y_batch.flatten())

                    y_pred.append(scores.flatten())

                    print(np.sum(np.not_equal(y_batch.flatten(), scores.flatten())) / float(y_batch.flatten().size))

                y_pred = np.asarray(y_pred)
                y_true = np.asarray(y_true)

                # y_pred=y_pred.flatten()
                y_pred = np.hstack(y_pred)
                y_true = np.hstack(y_true)
                y_pred = np.hstack(y_pred)
                y_true = np.hstack(y_true)
                # print(f"y_pred shape :{y_pred.shape}, y_true shape:{y_true.shape}")

                y_pred = y_pred[np.nonzero(y_true)]
                y_true = y_true[np.nonzero(y_true)]

                print(colored(f"f1 score for {current_corpus} is ", color='green', attrs=['bold']))
                f1 = f1_score(y_true, y_pred)

                # log f1 score
                self.f1_scores_values[4 - len(self.data_batch.eval_xml)] = f1

                analyze_incorrect_meanings(y_true, y_pred, self.data_batch.id2label, self.data_batch.meaning_dict)

    def pythonic_evaluate(self, restore, custom_sess=None, graph=None):

        idx = 0

        total = 0
        most_common = 0
        softmax = 0
        incorrect_common = 0

        # reset the eval data if empty
        if len(self.data_batch.eval_xml) == 0:
            self.data_batch.set_eval()

        with tf.Session(graph=graph, config=self.proto_config) as sess:

            if custom_sess:
                sess = custom_sess

            if restore:
                saver = tf.train.import_meta_graph(self.data_batch.params.MODEL_DIR + "model.meta")
                saver.restore(sess, self.data_batch.params.MODEL_DIR + "checkpoint")
            else:
                self.init.run()

            print("Evaluation...")

            # generate original sentence matrix for feeding
            for i in range(len(self.data_batch.eval_xml.keys())):
                # self.data_eval.generate_batch(corpus_names[0], self.params.batch_size)
                y_pred = []
                y_true = []
                current_corpus = self.data_batch.corpus_name

                # repeat until there are some sentences left
                while True:
                    # get the batch
                    batch_info = self.data_batch.generate_batch(self.params.batch_size, "eval",
                                                                self.params.use_meanings_mask)

                    # if the current corpus has no more sentence break
                    if isinstance(batch_info, int):
                        break

                    # unpack the infos
                    x_batch, y_batch, seq_len, no_meaning_seq, meaning_tensor, most_common_meanings, pos_tensor = batch_info

                    # initialize feed_dict
                    feed_dict = {self.word_ids: x_batch,
                                 self.no_meaning_seq: no_meaning_seq,
                                 self.meaning_tensor: meaning_tensor,
                                 self.pos_ids: pos_tensor,
                                 self.seq_len: seq_len}

                    feed_dict.update({k: v for k, v in zip(self.f1_scores_tensors, self.f1_scores_values)})

                    # Perform the run
                    scores = sess.run(
                        [self.scores],
                        feed_dict=feed_dict, )

                    idx += 1
                    scores = scores[0]
                    scores = apply_meaning_tensor(scores, meaning_tensor)
                    scores, t, mc, s, ic = apply_most_common_senses(scores,
                                                                    most_common_meanings,
                                                                    self.params.common_meaning_thrs,
                                                                    y_true=y_batch,
                                                                    debug=self.debug)

                    # log infos
                    total += t
                    most_common += mc
                    softmax += s
                    incorrect_common += ic

                    # remove the padded sequence
                    scores = list(scores)
                    for j in range(len(seq_len)):
                        scores[j] = scores[j][:seq_len[j]]

                    # remove the padded sequence
                    y_batch = list(y_batch)
                    for j in range(len(seq_len)):
                        y_batch[j] = y_batch[j][:seq_len[j]]

                    # flatten the two lists
                    y_batch = [item for sublist in y_batch for item in sublist]
                    scores = [item for sublist in scores for item in sublist]

                    # get the indices of the zero elements in the y_batch
                    zero_index = np.where(np.asarray(y_batch) == 0)[0]

                    # remove the indices from both the lists
                    y_batch = [y_batch[i] for i in range(len(y_batch)) if i not in zero_index]
                    scores = [scores[i] for i in range(len(scores)) if i not in zero_index]

                    # join them with the previous ones
                    y_true += y_batch
                    y_pred += scores

                    # print(sum([1 for t,p in zip(y_batch,scores) if t==p])/len(y_batch))

                print(colored(f"f1 score for {current_corpus} is ", color='green', attrs=['bold']))
                f1 = f1_score(y_true, y_pred)

                # log f1 score
                self.f1_scores_values[4 - len(self.data_batch.eval_xml)] = f1

                if self.debug:
                    analyze_incorrect_meanings(y_true, y_pred, self.data_batch.id2label, self.data_batch.meaning_dict,
                                               right_words=False)

            # perform perc calculation for softmax vs most_common
            softmax_perc = softmax / total
            most_common_mean = most_common / idx

            print(incorrect_common,softmax)
            try:
                incorrect_common=incorrect_common/softmax
            except ZeroDivisionError:
                incorrect_common=0

            print(incorrect_common)

            self.softmax_usage_val = softmax_perc
            self.most_common_usage_val = most_common_mean
            self.incorrect_common_val = incorrect_common

            # print(self.softmax_usage_val,self.most_common_usage_val)

    def test(self, restore, save_file):

        idx = 0

        total = 0
        most_common = 0
        softmax = 0
        incorrect_common = 0


        self.data_batch.save_test_file=save_file

        # reset the eval data if empty
        if len(self.data_batch.eval_xml) == 0:
            self.data_batch.set_eval()

        with tf.Session(graph=self.graph, config=self.proto_config) as sess:

            if restore:

                self.saver.restore(sess, "/home/dizzi/Work/PycharmProjects/NLP/HomeWork2/Good_Models/300_pos/Current_Model/Model/model-4")

            else:
                self.init.run()

            print("Testing...")

            # generate original sentence matrix for feeding
            # self.data_eval.generate_batch(corpus_names[0], self.params.batch_size)
            readable_sentences=[]
            y_true=[]

            # repeat until there are some sentences left
            # get the batch
            batch_info = self.data_batch.generate_batch(self.data_batch.test_len, "test",
                                                        self.params.use_meanings_mask)


            # unpack the infos
            x_batch, y_batch, seq_len, no_meaning_seq, meaning_tensor, most_common_meanings, pos_tensor = batch_info

            # initialize feed_dict
            feed_dict = {self.word_ids: x_batch,
                         self.no_meaning_seq: no_meaning_seq,
                         self.meaning_tensor: meaning_tensor,
                         self.pos_ids: pos_tensor}

            feed_dict.update({k: v for k, v in zip(self.f1_scores_tensors, self.f1_scores_values)})

            # Perform the run
            scores = sess.run(
                [self.scores],
                feed_dict=feed_dict, )

            idx += 1
            scores = scores[0]
            scores = apply_meaning_tensor(scores, meaning_tensor)
            scores, t, mc, s, ic = apply_most_common_senses(scores,
                                                            most_common_meanings,
                                                            self.params.common_meaning_thrs,
                                                            debug=self.debug)

            # log infos
            total += t
            most_common += mc
            softmax += s
            incorrect_common += ic

            # remove the padded sequence
            scores = list(scores)
            for j in range(len(seq_len)):
                scores[j] = scores[j][:seq_len[j]]

            # remove the padded sequence
            y_batch = list(y_batch)
            for j in range(len(seq_len)):
                y_batch[j] = y_batch[j][:seq_len[j]]


            for idx in range(len(scores)):

                read=self.data_batch.test_predict_to_sentence(scores[idx],y_batch[idx],no_meaning_seq[idx])

                readable_sentences.append(read)
                y_true.append(y_batch[idx])

        if not save_file:

            for pwrod, tword in zip(readable_sentences,y_true):
                print(" ".join(pwrod))
                print(" ".join(tword))
                print()
        else:

            total=0
            unk=0
            not_unk=0
            with open(self.data_batch.params.DATA_DIR+"test_asw", "w+") as file:
                for sentence in readable_sentences:
                    for doc_id, meaning_id in sentence:
                        to_write=f"{doc_id}\tbn:{meaning_id}\n"
                        file.write(to_write)

                        if meaning_id=="unk":
                            unk+=1
                        else:
                            not_unk+=1
                        total+=1

            print(f"Out of {total}, unk are {unk} ({round(unk/total*100,2)}), not unk are {not_unk} ({round(not_unk/total*100,2)})")


class AttentionWithContext(tf.layers.Layer):


    def __init__(self, input_shape,**kwargs):
        super(AttentionWithContext,self).__init__(**kwargs)
        self.build(input_shape)


    def build(self, input_shape):
        self.w=tf.Variable(tf.random_uniform(shape=(input_shape[-1],1),minval=-1,maxval=1))
        #self.b=tf.Variable(tf.zeros(shape=(input_shape[0],)))

    def __call__(self,H,mask):

        u=tf.tensordot(H,self.w,axes=1)
        u=tf.squeeze(u,-1)
       #u=u+self.b
        u=tf.tanh(u)


        a=tf.exp(u)

        if mask is not None:
            print(mask)
            mask=tf.squeeze(tf.cast(mask,tf.float32),-1)
            a=mask*a

        a/=tf.reduce_sum(a,axis=1,keep_dims=True)+0.0000001

        weighted_input=H*tf.expand_dims(a,-1)
        output=tf.reduce_sum(weighted_input,axis=1)
        return [output,a]