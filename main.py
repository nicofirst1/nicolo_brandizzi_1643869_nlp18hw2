from Classes.DataBatch import DataBatch
from Classes.Parameters import ModelParam, DataParam
from Classes.Model import Model
from utils import empty_dir, play_soud


mode="train"
restore=False
save_file=True

#initialize data class and parameter
data_param=DataParam()
data_batch=DataBatch(data_param,False)

#initializate the model class and parameter
param=ModelParam()
param.print()
param.write_params(data_param.INFOS_DIR)
model=Model(param,data_batch,param.debug)

if mode=="train":
    empty_dir(data_param.LOG_DIR)
    empty_dir(data_param.MODEL_DIR)
    model.init_graph()
    model.train()
elif mode=="test":
    model.init_graph()
    model.test(restore,save_file)

elif mode=="eval":
    model.init_graph()
    model.pythonic_evaluate(restore)

play_soud(repeat=4, freq=700)