# The problem

## Data

The original data is divided into two files:
- .xml: which contains elements such as *lemmas*, *pos* and *word-id*.
- each *word-id* is correlated with a *meaning-id* in the second file (*.txt*)

Moreover we have the **semantinc embeddings** given by the *babelfy_vector*.



### Approaches
* Create a set for all possible meanings
* use Attention mechanism 
* Create a dictionary to store POS2Int
* try to implement dropout
* get embeddings for both words and lemmas with related dictionaries
* Try to implement multitasking learning (maybe using POS as second task)
* Bring words to lower...yes

## Goal
The goal is to train a neural network to assign each word with a sense (the IDs int the second file).
This is a classification problem since the number of IDs is finite and discrete.

### Inputs
The mandatory inputs are sentences formed by **words ids** (that maps word embeddings) and the corresponding **meaning-id**

You may also use the following ones:
- **POS tags**
- **Lemmas**


### Outputs
The model must predict the meaning-id for a given word, disambiguating it

## Tips and Tricks
* Do not remove stopwords and punctuation
* use k-fold validation
* use decaying learning rate
* use F1 score


# TODO
- [X] Check if a word is not present in the embeddings, elaborate a strategy (found **unknown** vector)
- [X] Save each embedding from the original *babelfy_vector* only if the word is present in the xml file: this approach uses just 7% of the vectors in the 14Gb file, which 
are about 170 thousand words
- [X] Use semantic embeddings from [here](http://lcl.uniroma1.it/sensembed/)
- [X] Use dynamic padding
- [] Generate word embeddings using as input the meaning ids and the **sequence** of POS tags
- [X] Use  bi-GRU memory cell, performance are 10% worst 
- [X] Implement stack LSTM 

# Ossevations
- Only about 72% of words are found into the babelfy semantic embeddings

# Tuning 

smooth=0.685

Default parameter list:
- hidden_size = 512
- activation = tf.tanh
- forget_bias = 1.0
- use_gru = False
- embedding_size = 100
- batch_size = 16
- epochs = 1
- use_meanings=True
- learning_rate = 0.0001
- decay_rate = 0.00001
- decay_step = 100
- n_layer=3
- stack_layers=False
- lstm_peephole=False
- drop_out=False
- keep_prob=0.8

| Step          | Smoothed Value | Changes                              |      Notes              |
| ------------- |:--------------:| ------------------:                  | ---------------------:  |
| 2.240k        | 0.2259         | None                                 |                         |
| 4.500k        | 0.3654         | neurons=1024                         | f1 seems to saturate    |
| 2.240k        | 0.3097         | embedding_size=300                   |                         |
| 2.240k        | 0.2092         | use_gru=True                         |                         |
| 2.240k        | 0.2230         | activation=tf.nn.leaky_relu          |                         |
| 2.240k        | 0.2322         | activation=tf.nn.relu                |                         |
| 2.240k        | 0.2257         | forget_bias=0.8                      |                         |
| 2.240k        | 0.1174         | stack_layers=True                    |                         |
| 1.232k        | 0.0966         | stack_layers=True & n_layes=10       |more layers seems to only worsen the classification, stopped because it was too slow|
| 2.240k        | 0.2624         | lstm_peephole=True                   |                         |
| 2.240k        | 0.2158         | drop_out=True                        |                         |
| 2.240k        | 0.3548         | embedding_size=300                   |                         |
| 15k           | 0.4484         | embedding_size=400,     lstm_peephole=True, activation=tf.nn.relu  ,neurons=1024               |                         |
| 5.000k        | 0.4263         | embedding_size=400,     lstm_peephole=True, activation=tf.nn.relu  ,neurons=1024               |                         |
| 2.240k        | 0.2800         | scores = tf.nn.log_softmax(logits)   |                         |
| 4.500k        | 0.39272        | scores = tf.nn.log_softmax(logits)   |                         |
| 2.240k        | 0.3487        | scores = tf.nn.log_softmax(logits), self.lstm_peephole=True   |                         |
| 4.500k        | 0.4591        | previous +self.embedding_size = 700, self.hidden_size = 800 ,activation=tf.nn.relu,self.learning_rate = 0.001  |                         |


## Tunign qith different model architecture
Using dropout (0.85) and information from pos tags (use_pos=True), gives better resutls

# Helpful links
* [wsd git](https://github.com/Sshanu/Word-Sense-Disambiguation)
* [gru vs LSTM](https://arxiv.org/pdf/1412.3555v1.pdf)
* [LSTM for dummies](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
* [WSD param/architecture](https://pdfs.semanticscholar.org/7214/f8b81a7f5d78cfd808b3b873c013fe23bc1b.pdf)
* [dynamic bi-rnn](http://www.wildml.com/2016/08/rnns-in-tensorflow-a-practical-guide-and-undocumented-features/)
* [rnn](https://web.stanford.edu/class/cs20si/2017/lectures/slides_11.pdf)