#!/usr/bin/env python

from distutils.core import setup


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='Homework2',
      version='1.0',
      description='Word Disambiguation Model',
      author='Nicolò Brandizzi',
      author_email='brandizzi.1643869@studenti.uniroma1.it',
      install_requires=requirements,

     )