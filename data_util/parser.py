import xml.etree.ElementTree as etree

from utils import perc_print


def parse_text(xml_path):
    """
    Return the complete text for a xml file
    :param xml_path: a string to xml file
    :return:
    """

    assert xml_path.endswith('.xml')

    # create the xml parse tree
    corpus = etree.parse(xml_path).getroot()

    words = []

    # save each instance's text
    for par in corpus:
        for sent in par:
            for inst in sent:
                words.append(" ".join(inst.text.split()))

    return set(words)


def parse_meaning_dict(key_path):
    """
    Read the key file and return a dictioary
    :param key_path: the path to the kay file
    :return: dictionary of the form
        dict[word place]=meaning ID
    """

    res_dict = {}

    with open(key_path, "r+") as file:
        for line in file:
            res_dict[line.split()[0]] = line.split()[1].split(':')[1]

    print("Parsed meaning dictionary")

    return res_dict


def parse_xml_sentence(par, sentence_idx, key_dict):
    """
    Parse a sentence from a paraghraph
    :param par: the paraghraph
    :param sentence_idx: the index of the sentence in the paragraph
    :param key_dict: the dictioary of meaning keys
    :return: list of dictionaries where the keys are:
        word : the text of the word
        lemma : the lemma of the word
        pos : the pos tag
        id: (optional) the menaing id
    """

    words = []

    try:
        sent = par[sentence_idx]
    except IndexError:
        # if the sentences is not in the range o the current chapter,
        #  return zero
        return -1

    # get the words
    for word in sent:
        # ge the attributes
        attrib = word.attrib

        # look for the 'id' key
        if 'id' in attrib.keys():
            # if present replace the key place holder with the meaning ID
            attrib['id'] = key_dict[attrib['id']]

        attrib['word']=word.text
        # append element to the list
        words.append(attrib)

    # return the list plus another list
    return words


def parse_xml_par(xml_path, paragraph_idx, key_dict):
    """
    Parse a paraghrap in the xml file
    :param xml_path: the path to the xml file
    :param paragraph_idx: the paragraph index
    :param key_dict: the dictionary of meaning IDs
    :return: a list of list of words
    """
    assert xml_path.endswith('.xml')


    # create the xml parse tree
    corpus = etree.parse(xml_path).getroot()

    sentences = []

    # get the sentence list
    try:
        par = corpus[paragraph_idx]
    except IndexError:
        # if there are non parag left we reached the end of the document
        return -1

    for i in range(0, len(par)):
        sentences.append(parse_xml_sentence(par, i, key_dict))


    print(f"Parsed paragraph {paragraph_idx} ...")

    return sentences


def parse_babelfy(words_set, res_path="../DataSets/HW2/babelfy_vectors_filtered",embed_dim=400):
    """
    Parse just the embeddings with words in the words_set ad save them into a file
    :param words_set: a set of words
    :return: None
    """

    file_path = "../DataSets/HW2/babelfy_vectors"

    filtered_lines = []
    idx = 0
    j = 0
    with open(file_path, "r+") as file:
        for line in file:

            word = line.split()[0]

            # add the UNK embed
            if word == "unk":
                filtered_lines.append(line)
                continue

            # include words with no double sense wich are in the set
            if "_" not in word and word in words_set:
                filtered_lines.append(line)
                idx += 1


            else:
                # remove "_bn" from word
                word = word.replace("_bn", "")

                # replace _ with spaces
                word = word.replace("_", " ")

                # remove meaning from word
                word = word.split(":")[0]

                # add word to filtered if present in word set
                if word in words_set:
                    filtered_lines.append(line.replace("_bn", ""))
                    idx += 1

            j += 1

            perc_print(idx, j,msg="Words keep form original file")

    print(f"Found {idx} embeddings")


    #remove lines which have more elements that they should
    filtered_lines=[line for line in filtered_lines if len(line.split())==embed_dim+1]



    # save file
    with open(res_path, "w+") as file:
        file.writelines(filtered_lines)


def parse_class_dict(key_file_path):
    """
    Parse the key file and return a dict
    :param key_file_path: (str) the path to the kay file
    :return (dict): a dictionary which maps a meaning id to an int

    """

    classes=[]

    #read all the classes into a list
    with open(key_file_path,"r+") as file:

        for line in file:

            class_=line.split()[1].split(':')[1]
            classes.append(class_)

    #remove duplicates
    classes=list(set(classes))

    res_dict={}
    res_dict['unk']=0

    #fill the dict
    for c in classes:
        res_dict[c]=len(res_dict)


    return res_dict


def parse_lemma_prob_matrix(key_file_path, xml_file_path):
    """
    Parse all the possible meanings for all the words in the given dataset
    :param key_file_path: the path to the key file
    :param xml_file_path: the path to the xml file
    :return: a dict in which a key is a word and the value is a list of associated babelnet meaning ids for that word
    """


    classes={}
    #read all the classes into a list
    with open(key_file_path,"r+") as file:

        for line in file:

            class_=line.split()[1].split(':')[1]
            classes[line.split()[0]]=class_

    # create the xml parse tree
    corpus = etree.parse(xml_file_path).getroot()


    final_dict={}
    for document in corpus:

        for sentence in document:

            for word in sentence:

                if 'id' not in word.attrib.keys():
                    continue

                if word.attrib['lemma'] not in final_dict.keys():
                    final_dict[word.attrib['lemma']]=[]

                final_dict[word.attrib['lemma']].append(classes[word.attrib['id']])


    for k,v in final_dict.items():
        final_dict[k]=list(set(v))


    return final_dict