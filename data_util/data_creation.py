from utils import get_file_lines, perc_print, remove_lines_from_file
from data_util.parser import parse_text, parse_babelfy, parse_meaning_dict, parse_xml_par


def generate_embed_dict(filtered_file_name,xml_path, from_scratch=False):
    """
    Generate an embedding dictionary
    :param
    from_scratch: bool, if true create the filtered embed vectors file

    filtered_file_name: the file in which the embeddings are stored
        the file must be in the form
        word(:id) vector
        where (:id) is optional

    xml_path: the path to the xml file


    :return: a dictionary of the following structure
        word(:meaning)->embed vector,
        where meaning can be either "" or an ID
        embed vector is a vector of floats
    """

    if from_scratch:
        # get the set of words in the xml file
        words_set=parse_text(xml_path)

        # generate a filtered babelfy file
        parse_babelfy(words_set,res_path=filtered_file_name)

    embed_dict={}

    total=get_file_lines(filtered_file_name)

    remove_indices=[]

    # read the generated file
    with open(filtered_file_name,"r+") as file:

        idx=0

        #for every line save the word and the correstonding embed in the dictionary
        for line in file:
            word=line.split()[0]

            embed=line.split()[1:]
            new_embed=[]

            #some embeds are not castable to floats
            for elem in embed:
                try:
                    new_embed.append(float(elem))

                except ValueError:
                    remove_indices.append(idx)
                    print(f"error at line {idx}")
                    break


            embed_dict[word] = new_embed
            perc_print(idx,total,msg="embed dict generation")
            idx+=1

    #remove lines from file if there have been some errors
    if len(remove_indices)>0:
        remove_lines_from_file(filtered_file_name,remove_indices)
        embed_dict=generate_embed_dict(filtered_file_name,xml_path,from_scratch=False)



    print("Generated embeddings dictionary")

    return embed_dict


